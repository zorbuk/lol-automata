# LoL Automata #

Proyecto: https://bitbucket.org/zorbuk/lol-automata/src/main/LeagueAutomata/

Este proyecto usa reconocimiento por pixeles, texto y la api de riot games (https://developer.riotgames.com/docs/lol#game-client-api_live-client-data-api)

### Requisitos ###

* Ventana del Cliente a 1600*900

![Alt text](https://i.gyazo.com/4f1593474d6d098efcf27cf7fa666dbd.png)

* En el juego pantalla completa en modo ventana a 1920*1080
* Interfaz: tamaño del hud 20, tamaño del minimapa 50, en video activar modo daltonico.
* Hechizos: QWER DF 123567 4

![Alt text](https://i.gyazo.com/2562004556e68cf6aadd897f58f9e484.png)

* Autoataque habilitado.

### Setup ###

* Toda la configuracion necesaria la encontraras en el archivo 'config.cf'.